﻿namespace TicTacToe {

    public class TicTacToeGrid {
        private char[,]_grid;
        char[,] Grid {get; set;}
        
        public TicTacToeGrid() {
            char[,] Grid = { {' ', ' ', ' '}, {' ', ' ', ' ' }, {' ', ' ', ' ' }};
        }

        public void placeCharacter(char c, int row, int col) {
            do  {
                this._grid[row,col] = c;
                if (c != 'x' || c != 'o') {
                    Console.WriteLine("Invalid input, please enter x or y");
                    Console.ReadLine();
                }
            } while ((c != 'x' || c != 'o'));
        }

        public void printGrid() {
            foreach (char c in this._grid) {
            Console.WriteLine();
            }
        }

        public char checkRows() {
            for (int i = 0; i < _grid.GetLength(0); i++){
                char c = _grid[i,0];
                bool AllSame = true;
                for (int j = 0; j < _grid.GetLength(1); j++) {
                    if (c != _grid[i, j]) {
                        AllSame = false;
                    }
                }
                if (AllSame) {
                    return c;
                }

            }
        }   
            
    }
}

